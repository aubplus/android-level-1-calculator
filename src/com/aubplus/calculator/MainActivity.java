package com.aubplus.calculator;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends Activity {

	EditText aEditText;
	EditText bEditText;
	Spinner spinner;
	TextView resultTextView;
	Button getResultButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		aEditText = (EditText) findViewById(R.id.editTextA);
		bEditText = (EditText) findViewById(R.id.editTextB);
		resultTextView = (TextView) findViewById(R.id.resultText);

		// Array of operations
		String operations[] = { "A + B", "A - B", "A / B", "A * B" };

		// Selection of the spinner
		spinner = (Spinner) findViewById(R.id.operationsSpinner);

		// Application of the Array to the Spinner
		ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
				this, android.R.layout.simple_spinner_item, operations);
		spinnerArrayAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(spinnerArrayAdapter);

		getResultButton = (Button) findViewById(R.id.getResultButton);
		getResultButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				String aString = aEditText.getText().toString();
				String bString = bEditText.getText().toString();

				double aDouble = Double.parseDouble(aString);
				double bDouble = Double.parseDouble(bString);

				double result = -1;
				switch (spinner.getSelectedItemPosition()) {
				case 0:
					result = aDouble + bDouble;
					break;
				case 1:
					result = aDouble - bDouble;
					break;
				case 2:
					result = aDouble / bDouble;
					break;
				case 3:
					result = aDouble * bDouble;
					break;
				}

				resultTextView.setText("" + result);
			}
		});
	}
}
